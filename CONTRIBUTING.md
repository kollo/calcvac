Guide to contributing to CALCVAC/VACLINE
========================================

CALCVAC and VACLINE are commandline programs for Linux and WINDOWS. They have
been along since 2002 and have been used to design several vacuum systems of 
newly build accelerators, not only at DESY.

The port to Android (with quite a lot of improvements) now makes it more 
easy to construct and design a vacuum beamlich e.g. for accelerators.

So at the moment I would recommend to focus on the Android version of calcvac
for any improvements. 

However, changes in the base functionallity are planned to always be backported
into this repository, and if you like, we will try to keep it running on the
WINDOWS platform (however, this has always been tricky) and on debian linux.

WINDOWS support is depreciated. But if you are a WINDOWS expert and want to take
over... please do so.

Suggestions for improvements are therefor very welcome. Even the exchange 
of experience of usage could be very valuable.

The new version of CALCVAC/VACLINE is still file compatible (.inf and .vac. 
files, as well as the resulting .dat files with the calculated data). 
I would like to keep this as a feature even though the syntax could be improved
and cleaned up a bit.

Not everything runs moothly yet.

You can:
* report bugs and unexpected behaviour. --> open issue
* donate/share your vacuum beamline files for testing.
* discuss new features  --> open issue
* improve the user manual and online-help.


## License and attribution

All contributions must be properly licensed and attributed. If you are
contributing your own original work, then you are offering it under a CC-BY
license (Creative Commons Attribution). If it is code, you are offering it under
the GPL-v2. You are responsible for adding your own name or pseudonym in the
Acknowledgments file, as attribution for your contribution.

If you are sourcing a contribution from somewhere else, it must carry a
compatible license. The project was initially released under the GNU public
licence GPL-v2 which means that contributions must be licensed under open
licenses such as MIT, CC0, CC-BY, etc. You need to indicate the original source
and original license, by including a comment above your contribution. 


## Contributing with a Merge Request

The best way to contribute to this project is by making a merge request:

1. Login with your codeberg account or create one now
2. [Fork](https://codeberg.org/kollo/calcvac.git) the calcvac repository.  Work on your fork.
3. Create a new branch on which to make your change, e.g.
`git checkout -b my_code_contribution`, or make the change on the `new` branch.
4. Edit the file where you want to make a change or create a new file in the  `contrib` directory if you're not sure where your contribution might fit.
5. Edit `doc/ACKNOWLEGEMENTS` and add your own name to the list of contributors under the section with the current year. Use your name, or a codeberg ID, or a pseudonym.
6. Commit your change. Include a commit message describing the correction.
7. Submit a merge request against the calcvac repository.



## Contributing with an Issue

If you find a mistake and you're not sure how to fix it, or you don't know how
to do a merge request, then you can file an Issue. Filing an Issue will help us
see the problem and fix it.

Create a [new Issue](https://codeberg.org/kollo/calcvac/issues/new?issue) now!


## Thanks

We are very grateful for your support. With your help, this implementation
will be a great project. 

