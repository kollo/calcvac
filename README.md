<img alt="Calcvac/Vacline" src="logo/calcvac.png"/>

CALCVAC/VACLINE 
===============

    Copyright (c) 2002-2018 -- DESY Deutsches Elektronen Synchrotron, 
                 Ein Forschungszentrum der Helmholtz-Gemeinschaft
Authors:
--------
     Markus Hoffmann (2002-2018),
     Mike Seidel -2002

Description:
------------

The programs calcvac/vacline can calculate logitudinal one-dimensional pressure
profiles in vacuum pipes.  The pipes can consist of different sections of
different shape and material.  Outgasing of several different gas species can be
simulated. Also it is possible to install vacuum pumps  of different types. 
Also cryo effects (cold pipes and cryo pumps) are simulated. Finally the pipes 
can be linked together to form a network.

This program is scientific software.  The  used  methods  and  formulas  are 
published  in  DESY-HERA-03-23: Markus Hoffmann, "Vakuum-Simulationsrechnung
für HERA" (german).

Please see the manpages calcvac(1) and vacline(1) for details; 
INSTALL for compilation and installation info.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


Acknowledgements
================

Thanks to all people, who helped me to realize this package.

VACLINE uses functionallity of the LAPACK library. 
LAPACK (Linear Algebra Package) is a standard software library for 
numerical linear algebra. 
For details, see: http://www.netlib.org/lapack/


There is also a (more recent) version for Android available.
Please have a look at the repository Calcvac-Android at codeberg: 
https://codeberg.org/kollo/Calcvac-Android

#### Download

Sources and binary packages (see the releases section):

<a href="https://codeberg.org/kollo/calcvac/">
    <img alt="Get it on Codeberg" src="https://get-it-on.codeberg.org/get-it-on-blue-on-white.png" height="60">
</a>


<a href="https://f-droid.org/en/packages/de.drhoffmannsoftware.calcvac/">
<img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="80"></a>

