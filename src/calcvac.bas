' #################################################################################
' ## calcvac (c) Markus Hoffmann                         V.1.02     01.2009      ##
' ##                                                                             ##
' ##      calculates 1D vacuum distribution by given geometry                    ##
' ##                                                                             ##
' #################################################################################
' Letzte Bearbeitung 4.1.2009
' Letzte Bearbeitung 9.2.2012
' Letzte Bearbeitung 9.12.2012
' Letzte Bearbeitung 9.12.2013

' X11-Basic workarount mit ARRAY Zuweisung (geht im compiler nicht)

' Globale Einstellungen

CLR zeusana,writeout,h1ana
DIM negps(31),tsps(31),coldps(31),igpps(31)
DIM pdata(31,4096)
DIM coldout(31),stahlout(31),kupferout(31),aluminiumout(31)
DIM wolframout(31),albeout(31),negout(31),syliout(31)
DIM feuerout(31)

verbose=1
collect$=""

zeus_pumps$="ZEUS-pumps.dat"
h1_pumps$="H1-pumps.dat"
h1ana=TRUE
experiment$="H1"


' Test if vacline is usable

t$=SYSTEM$("vacline")

IF LEN(t$)<5
  PRINT "ERROR: vacline is not usable. Do you have it installed?"
  PRINT "Abort."
  QUIT
ENDIF

@kommandline

IF LEN(inputfile$)
  inffile$=inputfile$
ELSE
  PRINT "ERROR: you have not specified an input file. Nothing to do."
  PRINT "done."
  QUIT
ENDIF
IF EXIST(inffile$)
  IF verbose
    PRINT "<-- ";inffile$
  ENDIF
  @getsection_general(inffile$)
  @getsection_outgasing(inffile$)
  @getsection_pumping(inffile$)
  @getsection_measurements(inffile$)
  PRINT
  IF verbose
    PRINT "INFO: Title: ";title$
  ENDIF
ELSE
  PRINT "ERROR: Inputfile "+inffile$+" not found!"
  QUIT
ENDIF

'  Hier gehts los

IF zeusana
  experiment$="ZEUS"
  readings$=zeus_pumps$
  @do_analyse
  IF NOT writeout
    SYSTEM "rm -f event* "+experiment$+"-pumps.dat  test3a.dat"
  ENDIF
ENDIF
IF h1ana
  experiment$="H1"
  readings$=h1_pumps$
  @do_analyse
ENDIF
IF NOT writeout
  SYSTEM "rm -f event* *-pumps.dat  test3a.dat"
ENDIF
QUIT

PROCEDURE do_analyse
  PRINT inffile$+": Calculate pressure profiles for "+experiment$+" ";
  eee=MAX(0,@get_eventnr()-1)

  PRINT eee+1," Events."

  masse=2
  PRINT "Events: (";
  FLUSH
  FOR event=0 TO eee
    @make_vacline("event-"+str$(event)+"-"+str$(masse)+".vac")
  NEXT event
  PRINT "|";
  masse=16
  FOR event=0 TO eee
    @make_vacline("event-"+str$(event)+"-"+str$(masse)+".vac")
  NEXT event
  PRINT "|";
  masse=28
  FOR event=0 TO eee
    @make_vacline("event-"+str$(event)+"-"+str$(masse)+".vac")
  NEXT event
  PRINT ") ";eee
  PRINT "Specs: (";
  FLUSH
  FOR event=0 TO eee
    @addvac("event-"+str$(event)+"-2.dat","event-"+str$(event)+"-16.dat","test3a.dat")
    @addvac("test3a.dat","event-"+str$(event)+"-28.dat","event-"+str$(event)+".dat")
    @make_spektrum("event-"+str$(event)+"-2.dat","event-"+str$(event)+"-16.dat","event-"+str$(event)+"-28.dat","event"+str$(event))
    PRINT ".";
    FLUSH
  NEXT event
  PRINT ") ";eee
  PRINT "plot (";
  FLUSH
  @make_gnu
  PRINT ") "
RETURN

PROCEDURE make_spektrum(a$,b$,c$,d$)
  anzdata=0
  IF verbose
    PRINT "<-- ";a$
    PRINT "<-- ";b$
    PRINT "<-- ";c$
  ENDIF
  IF exist(a$) AND EXIST(b$) AND EXIST(c$)
    OPEN "I",#1,a$
    OPEN "I",#2,b$
    OPEN "I",#3,c$
    WHILE not (eof(#1) OR eof(#2) OR eof(#3))
      LINEINPUT #1,t1$
      LINEINPUT #2,t2$
      LINEINPUT #3,t3$
      IF left$(TRIM$(t1$))="%"
        '  print t1$
      ELSE
        t1$=TRIM$(t1$)
        t'2$=TRIM$(t2$)
        t3$=TRIM$(t3$)
        SPLIT t1$," ",0,a1$,t1$
        SPLIT t2$," ",0,a2$,t2$
        SPLIT t3$," ",0,a3$,t3$
        pos=VAL(a1$)
        SPLIT t1$," ",0,a1$,t1$
        SPLIT t2$," ",0,a2$,t2$
        SPLIT t3$," ",0,a3$,t3$
        p1=VAL(a1$)
        p2=VAL(a2$)
        p3=VAL(a3$)
        pdata(0,anzdata)=pos
        pdata(2,anzdata)=p1
        pdata(16,anzdata)=p2
        pdata(28,anzdata)=p3
        INC anzdata
      ENDIF
    WEND
    CLOSE #1
    CLOSE #2
    CLOSE #3
  ENDIF
  IF verbose
    PRINT "--> ";d$
  ENDIF

  OPEN "O",#1,experiment$+"-"+d$+"-spektrum.dat"
  PRINT #1,"% Ausgabe von calcvac von "+env$("USER")+" "+date$+" "+time$
  PRINT #1,"% Experiment: "+experiment$
  PRINT #1,"% "+d$
  PRINT #1,"% "
  PRINT #1,"% Partialdruecke fuer H2(2), CH4(16) und CO(28)"
  PRINT #1,"% "
  PRINT #1,"% pos P[H2] P[CH4] P[CO] P[gesamt]"
  FOR i=0 TO anzdata-1
    PRINT #1,pdata(0,i);" ";pdata(2,i);" ";pdata(16,i);" ";pdata(28,i);" ";pdata(2,i)+pdata(16,i)+pdata(28,i);" "
  NEXT i
  CLOSE #1
RETURN

PROCEDURE make_gnu
  IF verbose
    PRINT "--> vakuumplot.gnu"
    PRINT "--> ";experiment$+"-vakuum.ps"
  ENDIF
  OPEN "O",#1,"vakuumplot.gnu"
  PRINT #1,"# Ausgabe von calcvac von "+env$("USER")+" "+date$+" "+time$
  PRINT #1,"# Experiment: "+experiment$
  PRINT #1,"# "
  PRINT #1,"set term post"
  PRINT #1,"set term post enhanced "
  PRINT #1,"set term post portrait"
  PRINT #1,"set term post color"
  PRINT #1,"set term post solid 16"
  PRINT #1,"set output "+CHR$(34)+experiment$+"-vakuum.ps"+CHR$(34)
  @gnu_usercommands
  PRINT #1,"set multiplot"
  PRINT #1,"set size 1,0.66"
  PRINT #1,"set key left top"
  PRINT #1,"set origin 0,0.33"
  PRINT #1,"set ylabel 'total pressure P[mbar]'"
  PRINT #1,"set logscale y"
  PRINT #1,"set label '"+experiment$+"' at 15,2e-8"
  PRINT #1,"set yrange [1e-11:1e-6]"
  PRINT #1,"set label '"+title$+"' at -5,5e-7"
  PRINT #1,"set label 'vacline V.2.00' at -20,1.2e-11"
  PRINT #1,"set format y '%5.1e'"
  PRINT #1,"set arrow from 0,3e-11 to 0,2e-10 lt 2 lw 2"
  PRINT #1,"set label 'IP' at 0,2e-11"
  IF experiment$="ZEUS"
    PRINT #1,"set arrow from -5.8,1e-8 to -5.8,3e-9 nohead lt 3"
    PRINT #1,"set arrow from 5.8,1e-8 to 5.8,3e-9 nohead lt 3"
    PRINT #1,"set arrow from -2,1e-8 to -2,3e-9 nohead lt 1"
    PRINT #1,"set arrow from 2,1e-8 to 2,3e-9 nohead lt 1"
  ENDIF
  PRINT #1,"plot 'event-0.dat' u 1:2 ti 'no event' w l lt 1 lw 2 \"
  FOR i=1 TO eee
    PRINT #1,", 'event-"+STR$(i)+".dat' u 1:2 ti 'Event "+STR$(i)+"' w l lt 3 lw 2 \"
  NEXT i
  FOR i=0 TO eee
    PRINT #1,", '"+readings$+"' u 1:"+STR$(2+i)+" t '' w p  \"
  NEXT i
  PRINT #1," "
  PRINT #1,"set ylabel '[l m/s]'"
  PRINT #1,"set size 1,0.16"
  PRINT #1,"set origin 0,0.16"
  PRINT #1,"set key top"
  PRINT #1,"set nolog"
  PRINT #1,"set nolabel"
  PRINT #1,"set yrange [0:250]"
  PRINT #1,"set label 'conductance' at -19,200"
  PRINT #1,"set format y '%7.7g'"
  PRINT #1,"plot 'event-0-2.dat' u 1:4 ti 'H_2' w steps lt 3 lw 2, \"
  PRINT #1,"     'event-0-16.dat' u 1:4 ti 'CH_4' w steps lt 2 lw 2, \"
  PRINT #1,"     'event-0-28.dat' u 1:4 ti 'CO' w steps lt 1 lw 2"
  PRINT #1,"set ylabel '[mbar l/s]'"
  PRINT #1,"set xlabel 'z [m]'"
  PRINT #1,"set size 1,0.16"
  PRINT #1,"set origin 0,0"
  PRINT #1,"set key top"
  PRINT #1,"set nologscale y"
  PRINT #1,"set nolabel"
  PRINT #1,"set yrange [-1e-7:1e-7]"
  PRINT #1,"set label 'gas flow' at -19,8e-9"
  PRINT #1,"set format y '%7.7g'"
  PRINT #1,"set grid"
  PRINT #1,"plot 'event-0-2.dat' u 1:3 ti 'H_2' w l lt 3 lw 2, \"
  PRINT #1,"     'event-0-16.dat' u 1:3 ti 'CH_4' w l lt 2 lw 2, \"
  PRINT #1,"     'event-0-28.dat' u 1:3 ti 'CO' w l lt 1 lw 2 "
  PRINT #1,"set nomulti"

  FOR ooo=0 TO eee
    PRINT ".";
    FLUSH
    @gnuplot_seite(ooo)
  NEXT ooo
  @gnuplot_phasespace(eee)
  CLOSE #1
  IF verbose OR writeout
    SYSTEM "gnuplot vakuumplot.gnu"
  ELSE
    SYSTEM "gnuplot vakuumplot.gnu ; rm -f vakuumplot.gnu"
  ENDIF
RETURN
PROCEDURE gnuplot_seite(enr)
  PRINT #1,"set nolabel"
  PRINT #1,"set xlabel ''"
  PRINT #1,"set multi"
  PRINT #1,"set size 1,0.66"
  PRINT #1,"set key left top"
  PRINT #1,"set origin 0,0.33"
  PRINT #1,"set ylabel 'P[mbar]'"
  PRINT #1,"set logscale y"
  PRINT #1,"set label '"+experiment$+"' at 15,2e-8"
  PRINT #1,"set yrange [1e-11:1e-6]"
  action$=@get_event$(LEFT$(experiment$)+STR$(enr))
  PRINT #1,"set label 'Event #"+STR$(enr)+": "+ecomment$+"' at -6,5e-7"
  PRINT #1,"set label 'vacline V.2.00' at -20,1.2e-11"
  PRINT #1,"set format y '%5.1e'"
  PRINT #1,"set arrow from 0,3e-11 to 0,2e-10 lt 2 lw 2"
  PRINT #1,"set label 'IP' at 0,2e-11"
  IF experiment$="ZEUS"
    PRINT #1,"set arrow from -5.8,1e-8 to -5.8,3e-9 nohead lt 3"
    PRINT #1,"set arrow from 5.8,1e-8 to 5.8,3e-9 nohead lt 3"
    PRINT #1,"set arrow from -2,1e-8 to -2,3e-9 nohead lt 1"
    PRINT #1,"set arrow from 2,1e-8 to 2,3e-9 nohead lt 1"
  ENDIF
  f$="event"+STR$(enr)
  PRINT #1,"plot '"+experiment$+"-"+f$+"-spektrum.dat' u 1:2 ti 'H_2' w l lt 3 lw 2 \"
  PRINT #1,", '"+experiment$+"-"+f$+"-spektrum.dat' u 1:3 ti 'CH_4' w l lt 2 lw 2 \"
  PRINT #1,", '"+experiment$+"-"+f$+"-spektrum.dat' u 1:4 ti 'CO' w l lt 1 lw 2 \"
  PRINT #1,", '"+experiment$+"-"+f$+"-spektrum.dat' u 1:5 ti 'sum' w l lt 5 lw 2 \"
  PRINT #1,", '"+readings$+"' u 1:"+STR$(2+enr)+" t '' w p lt 4"
  PRINT #1,"set ylabel '[l/s m]'"
  PRINT #1,"set size 1,0.16"
  PRINT #1,"set origin 0,0.16"
  PRINT #1,"set key top"
  PRINT #1,"set nolog"
  PRINT #1,"set nolabel"
  PRINT #1,"set yrange [0:850]"
  PRINT #1,"set label 'pumping speeds' at -19,700"
  PRINT #1,"set format y '%7.7g'"
  PRINT #1,"plot 'event-"+STR$(enr)+"-2.dat' u 1:6 ti 'H_2' w steps lt 3 lw 2, \"
  PRINT #1,"     'event-"+STR$(enr)+"-16.dat' u 1:6 ti 'CH_4' w steps lt 2 lw 2, \"
  PRINT #1,"     'event-"+STR$(enr)+"-28.dat' u 1:6 ti 'CO' w steps lt 1 lw 2"
  PRINT #1,"set ylabel '[l/s m]'"
  PRINT #1,"set xlabel 'z [m]'"
  PRINT #1,"set size 1,0.16"
  PRINT #1,"set origin 0,0"
  PRINT #1,"set key top"
  PRINT #1,"set logscale y"
  PRINT #1,"set nolabel"
  PRINT #1,"set yrange [1e-11:1e-6]"
  PRINT #1,"set label 'outgasing rates' at -19,8e-8"
  PRINT #1,"set format y '%7.7g'"
  PRINT #1,"plot 'event-"+STR$(enr)+"-2.dat' u 1:5 ti 'H_2' w steps lt 3 lw 2, \"
  PRINT #1,"     'event-"+STR$(enr)+"-16.dat' u 1:5 ti 'CH_4' w steps lt 2 lw 2, \"
  PRINT #1,"     'event-"+STR$(enr)+"-28.dat' u 1:5 ti 'CO' w steps lt 1 lw 2 "
  PRINT #1,"set nomulti"
RETURN
PROCEDURE gnuplot_phasespace(nmax)
  LOCAL enr
  PRINT #1,"set term post enh col sol 8"
  PRINT #1,"set nolabel"
  PRINT #1,"set noarrow"
  PRINT #1,"set xlabel ''"
  PRINT #1,"set multi"
  PRINT #1,"set logscale x"
  PRINT #1,"set nologscale y"
  PRINT #1,"set autoscale"
  PRINT #1,"set size 0.5,0.35"
  PRINT #1,"set xlabel 'P[mbar]'"
  PRINT #1,"set ylabel 'gas flow [mbar l/s]'"
  PRINT #1,"set key left top"
  PRINT #1,"set title 'vacuum phase space'"
  FOR enr=0 TO eee
    PRINT #1,"set origin "+STR$(0.5*abs(odd(enr)))+","+STR$(INT(enr/2)*0.3)
    action$=@get_event$(LEFT$(experiment$)+STR$(enr))
    PRINT #1,"plot 'event-"+STR$(enr)+"-2.dat' u 2:3 ti '+/- 20 m' w l lt 4 lw 2, \"
    PRINT #1," 'event-"+STR$(enr)+"-2.dat' u 2:((abs($1)<=6)?($3):1/0) ti '+/- 6 m' w l lt 2 lw 2, \"
    PRINT #1," 'event-"+STR$(enr)+"-2.dat' u 2:((abs($1)<=3)?($3):1/0) ti '+/- 3 m' w l lt 3 lw 2, \"
    PRINT #1," 'event-"+STR$(enr)+"-2.dat' u 2:((abs($1)<=1)?($3):1/0) ti '+/- 1 m' w l lt 1 lw 2"
  NEXT enr
  PRINT #1,"set nomulti"
RETURN

PROCEDURE process_form(r$)
  LOCAL mat$,form$
  l=VAL(@get_value$(r$,"L"))  ! Defaultwerte
  q=VAL(@get_value$(r$,"Q"))
  w=VAL(@get_value$(r$,"W"))*sqr(28/masse)
  a=VAL(@get_value$(r$,"A"))

  ' Material

  mat$=@get_value$(r$,"MAT")
  IF mat$="STAHL" OR mat$="STEEL"
    q=stahlout(masse)
  ELSE if mat$="AL"
    q=aluminiumout(masse)
  ELSE if mat$="KUPFER" OR mat$="COPPER" OR mat$="CU"
    q=kupferout(masse)
  ELSE if mat$="ALBE"
    q=albeout(masse)
  ELSE if mat$="WOLFRAM"
    q=wolframout(masse)
  ELSE
    PRINT "Unknown material. "+mat$
    STOP
  ENDIF

  ' Form

  form$=@get_value$(r$,"FORM")
  IF form$="ELLIPT"
    b=VAL(@get_value$(r$,"B"))
    w=@leitwert_ellipt(a,b,1,masse)
    a=@umfang_ellipt(a,b)
  ELSE if form$="RECT"
    b=VAL(@get_value$(r$,"B"))
    w=@leitwert_rechteck(a,b,1,masse)
    a=@umfang_rechteck(a,b)
  ELSE if form$="CIRC"
    d=VAL(@get_value$(r$,"D"))
    w=@leitwert_rund(d,1,masse)
    a=INT(pi*d*100*100)
  ELSE if form$="SCHLUESSELLOCH"
    b=VAL(@get_value$(r$,"B"))
    c=VAL(@get_value$(r$,"C"))
    d=VAL(@get_value$(r$,"D"))
    w=@leitwert_schluesselloch(a,b,c,d,1,masse)
    a=@umfang_schluesselloch(a,b,c,d)
  ELSE if LEN(form$)
    PRINT "Unknown form. "+form$
    STOP
  ENDIF
RETURN

PROCEDURE make_vacline(file$)
  PRINT "--> "+file$
  OPEN "O",#1,file$
  PRINT #1,"! Generated by calcvac (c) Markus Hoffmann "+date$+" "+time$
  PRINT #1,"! Experiment: "+experiment$+" Masse: "+STR$(masse)+" Event #"+STR$(event)
  PRINT #1
  PRINT #1,"TITLE "+ENCLOSE$(title$+" "+experiment$+" mass= "+STR$(masse))
  PRINT #1
  OPEN "I",#4,inffile$
  fsec=0

  WHILE NOT EOF(#4)
    LINEINPUT #4,tt$
    t$=UPPER$(TRIM$(tt$))
    IF left$(t$)="["
      s$=LEFT$(t$,LEN(t$)-1)
      s$=RIGHT$(s$,LEN(s$)-1)

      IF s$="LATTICE"
        fsec=TRUE
      ELSE if s$="EVENTS"
        BREAK
      ELSE if s$="COMMON" OR s$=UPPER$(experiment$)
        fsec=TRUE
      ELSE
        fsec=0
      ENDIF
    ELSE if LEFT$(t$)="#"
      ' nixtun
    ELSE if LEFT$(t$,4)="LINK"
      PRINT #1,tt$
    ELSE if LEN(t$)
      IF fsec
        SPLIT t$,":",1,name$,b$
        a$=REPLACE$(b$," = ","=")
        a$=REPLACE$(a$,","," ")
        a$=TRIM$(a$)
        SPLIT a$," ",1,ty$,r$
        IF ty$="START" OR ty$="END"
          PRINT #1,tt$
        ELSE if LEFT$(ty$,4)="LINE"
          PRINT #1,tt$
        ELSE if LEFT$(ty$,4)="SECT"
          PRINT #1,tt$
        ELSE if LEFT$(ty$,4)="ROHR" OR LEFT$(ty$,4)="TUBE"
          @process_form(r$)
          PRINT #1,name$+": SECTION, L="+STR$(l)+", q="+STR$(q)+", W="+STR$(w)+", A="+STR$(a)
        ELSE if LEFT$(ty$,4)="PUMP"
          ps=VAL(@get_value$(r$,"PS"))
          @process_form(r$)

          typ$=@get_value$(r$,"TYP")
          IF typ$="NEG"
            ADD q,negout(masse)
            speed=VAL(@get_value$(r$,"SPEED"))
            IF speed
              ps=speed*negps(masse)/l
            ENDIF
          ELSE if typ$="IGP"
            speed=VAL(@get_value$(r$,"SPEED"))
            IF speed
              ps=speed*igpps(masse)/l
            ENDIF
          ELSE if typ$="TSP"
            speed=VAL(@get_value$(r$,"SPEED"))
            IF speed
              ps=speed*tsps(masse)/l
            ENDIF
          ELSE if typ$="COLD"
            ADD q,coldout(masse)
            t$=@get_value$(r$,"T")
            IF len(t$)
              MUL w,sqrt(VAL(t$)/300)
            ENDIF
            speed=VAL(@get_value$(r$,"SPEED"))
            IF speed
              ps=speed*coldps(masse)/l
            ENDIF
          ELSE
            PRINT "ERROR: Unknown pumptyp. "+typ$
            STOP
          ENDIF
          action$=@get_value$(r$,"ACTION")
          WHILE len(action$)
            SPLIT action$,"|",1,a$,action$
            opc:
            SPLIT a$,"(",0,a$,arg$
            IF len(arg$)
              IF right$(a$)=")"
                arg$=LEFT$(arg$,LEN(arg$)-1)
              ENDIF
            ELSE
              arg$="1"
            ENDIF
            IF a$="SYLI"
              ADD q,syliout(masse)*val(arg$)
            ELSE if a$="NONE"
            ELSE if a$="OFF"
              ps=0
            ELSE if a$="HEAT"
              ADD q,feuerout(masse)*val(arg$)
            ELSE
              IF left$(a$)=LEFT$(experiment$)
                IF event=VAL(RIGHT$(a$))
                  a$=UPPER$(@get_event$(a$))
                  GOTO opc
                ENDIF
              ELSE
                PRINT "unknown action: "+a$
              ENDIF
            ENDIF
          WEND
          PRINT #1,name$+": SECTION, L="+STR$(l)+", q="+STR$(q)+", W="+STR$(w)+", A="+STR$(a)+", Ps="+STR$(ps)
        ELSE
          PRINT "unresolved: T$=";t$,fsec
        ENDIF
      ENDIF
    ENDIF
  WEND

  PRINT #1,"USE, "+experiment$+"line"
  CLOSE #1
  CLOSE #4

  IF not writeout
    file$=file$+" ; rm -f "+file$
  ENDIF
  IF verbose>1
    PRINT SYSTEM$("vacline "+collect$+" "+file$)
  ELSE
    ~SYSTEM$("vacline "+collect$+" "+file$)
    PRINT ".";
  ENDIF
  FLUSH
RETURN
FUNCTION leitwert_schluesselloch(a,b,c,d,l,m)
  RETURN @leitwert_ellipt(a,b,l,m)+@leitwert_rechteck(c,d,l,m)
ENDFUNC
FUNCTION umfang_schluesselloch(a,b,c,d)
  RETURN 2*c*100*100+@umfang_ellipt(a,b)
ENDFUNC
FUNCTION kk(x)
  RETURN 0.0653947/(x+0.0591645)+1.0386
ENDFUNC
' Leitwert fuer eine Elliptische Kammer der Laenge l fuer ein
' Gas der Molekuelmassenzahl m

FUNCTION leitwert_rund(a,l,m)
  LOCAL c1,c2,c3
  MUL a,100
  MUL l,100
  c1=11.6*a*a*pi/4
  c2=17.1*a*a*a*a/sqrt(a*a+a*a)/l
  c3=1/(1/c1+1/c2)
  RETURN int(sqrt(28/m)*c3*100)/100   ! in l/s
ENDFUNC
FUNCTION leitwert_ellipt(a,b,l,m)
  LOCAL c1,c2,c3
  a=a*100
  b=b*100
  l=l*100
  c1=11.6*a*b*pi/4
  c2=17.1*a*a*b*b/sqrt(a*a+b*b)/l
  c3=1/(1/c1+1/c2)
  RETURN int(sqrt(28/m)*c3*100)/100   ! in l/s
ENDFUNC
FUNCTION leitwert_rechteck(a,b,l,m)
  LOCAL c1,c2,c3
  MUL a,100
  MUL b,100
  MUL l,100
  c1=11.6*a*b
  c2=30.9*@kk(MIN(a/b,b/a))
  MUL c2,a*a*b*b/(a+b)/l
  c3=1/(1/c1+1/c2)
  RETURN int(sqrt(28/m)*c3*100)/100   ! in l/s
ENDFUNC

FUNCTION umfang_ellipt(ua,ub)
  LOCAL eprod,oprod,i,aa,bb,eps,sum
  IF ub>ua
    aa=ub
    ub=ua
    ua=aa
  ENDIF
  aa=ua/2
  bb=ub/2
  eprod=1
  oprod=1
  eps=(aa*aa-bb*bb)/aa/aa
  sum=1
  FOR i=1 TO 10
    MUL oprod,2*i-1
    MUL eprod,2*i
    ADD sum,-1/(2*i-1)*eps^i*(oprod/eprod)^2
  NEXT i
  ' Genauigkeit etwa 1e-4
  RETURN int(ua*pi*sum*100*100)   ! in cm^2/m
ENDFUNC
FUNCTION umfang_rechteck(a,b)
  RETURN int(2*(a+b)*100*100)   ! in cm^2/m
ENDFUNC

PROCEDURE addvac(a$,b$,f$)
  IF not EXIST(a$)
    PRINT "ERROR: "+a$+" not found."
  RETURN
ENDIF
IF not EXIST(b$)
  PRINT "ERROR: "+b$+" not found."
RETURN
ENDIF
OPEN "I",#1,a$
OPEN "I",#2,b$
OPEN "O",#3,f$
PRINT #3,"% Ausgabe von calcvac (c) Markus Hoffmann "+date$+" "+time$
PRINT #3,"% Vac-Add: "+a$+"+"+b$
WHILE not eof(#1)
  LINEINPUT #1,t1$
  LINEINPUT #2,t2$
  IF left$(TRIM$(t1$))="%"
    PRINT #3,t1$
  ELSE
    t1$=TRIM$(t1$)
    t2$=TRIM$(t2$)
    SPLIT t1$," ",0,a1$,t1$
    SPLIT t2$," ",0,a2$,t2$
    pos=VAL(a1$)
    SPLIT t1$," ",0,a1$,t1$
    SPLIT t2$," ",0,a2$,t2$
    p1=VAL(a1$)
    p2=VAL(a2$)
    PRINT #3,pos'p1+p2't2$
  ENDIF
WEND
CLOSE #1
CLOSE #2
CLOSE #3
RETURN
PROCEDURE intro
  PRINT "calcvac V.2.00 (c) Markus Hoffmann 2002-2012"
  VERSION
RETURN
PROCEDURE usage
  PRINT
  PRINT "Usage:"
  PRINT "======"
  PRINT "calcvac [options] inputfile"
  PRINT
  PRINT "Options:"
  PRINT "========"
  PRINT "--H1                -- do calculation for H1"
  PRINT "--ZEUS              -- do calculation for ZEUS"
  PRINT "-v                  -- be more verbose"
  PRINT "-q                  -- be less verbode"
  PRINT "-r <number>         -- specify resolution"
  PRINT "-n <number>         -- specify max number of output points"
  PRINT "--ellipt <a> <b>    -- calculate leitwert of an elliptical chamber"
  PRINT "--version           -- show version information"
  PRINT "-w                  -- writeout data (intermediate files )"
  PRINT "-o <filename>       -- place output to file [";outputfilename$;"]"
RETURN
PROCEDURE kommandline
  LOCAL i,t$
  i=1
  WHILE len(PARAM$(i))
    IF left$(PARAM$(i))="-"
      IF param$(i)="--help" OR PARAM$(i)="-h"
        @intro
        @usage
        QUIT
      ELSE if PARAM$(i)="--ellipt"
        INC i
        a=VAL(PARAM$(i))
        INC i
        b=VAL(PARAM$(i))
        PRINT "Umfang: ";@umfang_ellipt(a,b)
        PRINT "Leitwert: ";@leitwert_ellipt(a,b,1,28)
        QUIT
      ELSE if PARAM$(i)="--version"
        @intro
        QUIT
      ELSE if PARAM$(i)="--ZEUS"
        zeusana=true
        experiment$="ZEUS"
      ELSE if PARAM$(i)="--H1"
        h1ana=true
        experiment$="H1"
      ELSE if PARAM$(i)="-w"
        writeout=true
      ELSE if PARAM$(i)="-v"
        INC verbose
        collect$=collect$+PARAM$(i)+" "
      ELSE if PARAM$(i)="-q"
        DEC verbose
        collect$=collect$+PARAM$(i)+" "
      ELSE if PARAM$(i)="-o"
        INC i
        IF len(PARAM$(i))
          outputfilename$=PARAM$(i)
        ENDIF
      ELSE if PARAM$(i)="-n"
        collect$=collect$+PARAM$(i)+" "
        INC i
        collect$=collect$+PARAM$(i)+" "
      ELSE if PARAM$(i)="-r"
        collect$=collect$+PARAM$(i)+" "
        INC i
        collect$=collect$+PARAM$(i)+" "
      ELSE
        collect$=collect$+PARAM$(i)+" "
      ENDIF
    ELSE
      IF PARAM$(i)="xbasic" OR PARAM$(i)="calcvac.bas" OR PARAM$(i)="calcvac"
        ' Do nothing
      ELSE
        inputfile$=PARAM$(i)
        IF not EXIST(inputfile$)
          PRINT "calcvac: "+inputfile$+": file or path not found"
          CLR inputfile$
        ENDIF
      ENDIF
    ENDIF
    INC i
  WEND
  IF i=1
    @intro
    @usage
  ENDIF
RETURN
PROCEDURE getsection_outgasing(f$)
  LOCAL t$,fsec,a$,b$,p$,i
  DIM datt(31)
  fsec=0
  IF exist(f$)
    OPEN "I",#1,f$
    WHILE not eof(#1)
      LINEINPUT #1,t$
      t$=UPPER$(TRIM$(t$))
      IF left$(t$)="["
        fsec=0
        s$=LEFT$(t$,LEN(t$)-1)
        s$=RIGHT$(s$,LEN(s$)-1)
        IF s$="OUTGASING"
          PRINT "[OUTGASING]";
          FLUSH
          fsec=true
        ENDIF
      ELSE if LEFT$(t$)="#"
      ELSE if LEN(t$)
        IF fsec
          SPLIT t$,":",1,p$,t$
          WHILE len(t$)
            SPLIT t$," ",1,a$,t$
            SPLIT a$,"(",1,b$,a$
            a$=LEFT$(a$,LEN(a$)-1)
            IF a$="DEFAULT"
              FOR i=0 TO 30
                datt(i)=VAL(b$)
              NEXT i
            ELSE
              datt(VAL(a$))=VAL(b$)
            ENDIF
          WEND
          IF p$="STEEL"
            stahlout()=datt()
          ELSE IF p$="WOLFRAM"
            wolframout()=datt()
          ELSE IF p$="CU"
            kupferout()=datt()
          ELSE IF p$="AL"
            aluminiumout()=datt()
          ELSE IF p$="ALBE"
            albeout()=datt()
          ELSE IF p$="NEG"
            negout()=datt()
          ELSE IF p$="TSPHEAT"
            feuerout()=datt()
          ELSE IF p$="SYLI"
            syliout()=datt()
          ENDIF
        ENDIF
      ENDIF
    WEND
    CLOSE #1
  ENDIF
  '  erase datt()
RETURN
PROCEDURE getsection_pumping(f$)
  LOCAL t$,fsec,a$,b$,p$,i
  DIM datt(31)
  fsec=0
  IF EXIST(f$)
    OPEN "I",#1,f$
    WHILE NOT EOF(#1)
      LINEINPUT #1,t$
      t$=UPPER$(TRIM$(t$))
      IF LEFT$(t$)="["
        fsec=0
        s$=LEFT$(t$,LEN(t$)-1)
        s$=RIGHT$(s$,LEN(s$)-1)
        IF s$="PUMPING"
          PRINT "[PUMPING]";
          FLUSH
          fsec=TRUE
        ENDIF
      ELSE IF LEFT$(t$)="#"
      ELSE IF LEN(t$)
        IF fsec
          SPLIT t$,":",1,p$,t$
          WHILE LEN(t$)
            SPLIT t$," ",1,a$,t$
            SPLIT a$,"(",1,b$,a$
            a$=LEFT$(a$,LEN(a$)-1)
            IF a$="DEFAULT"
              FOR i=0 TO 30
                datt(i)=VAL(b$)
              NEXT i
            ELSE
              datt(VAL(a$))=VAL(b$)
            ENDIF
          WEND
          IF p$="COLD" OR p$="CRYO"
            coldps()=datt()
          ELSE IF p$="TSP"
            tsps()=datt()
          ELSE IF p$="IGP"
            igpps()=datt()
          ELSE IF p$="NEG"
            negps()=datt()
          ENDIF
        ENDIF
      ENDIF
    WEND
    CLOSE #1
  ENDIF
  ' ERASE datt()
RETURN
PROCEDURE getsection_general(f$)
  LOCAL t$,fsec,a$,b$,p$,i
  fsec=0
  IF EXIST(f$)
    IF verbose
      PRINT "<-- ";f$;" [general]"
    ENDIF
    OPEN "I",#1,f$
    WHILE NOT EOF(#1)
      LINEINPUT #1,t$
      t$=TRIM$(t$)
      IF LEFT$(t$)="["
        fsec=0
        s$=LEFT$(t$,LEN(t$)-1)
        s$=RIGHT$(s$,LEN(s$)-1)
        IF UPPER$(s$)="GENERAL"
          PRINT "[GENERAL]";
          FLUSH
          fsec=TRUE
        ENDIF
      ELSE if LEFT$(t$)="#"
      ELSE if LEN(t$)
        SPLIT t$,"=",1,p$,t$
        IF upper$(p$)="TITLE"
          title$=t$
        ELSE if UPPER$(p$)="ZREADINGS"
          zeus_pumps$=t$
        ELSE if UPPER$(p$)="HREADINGS"
          h1_pumps$=t$
        ENDIF
      ENDIF
    WEND
    CLOSE #1
  ENDIF
RETURN
PROCEDURE gnu_usercommands
  PRINT #1,"# Usercommands:"
  @getsection_plot(inffile$)
  PRINT #1,"# End of Usercommands"
RETURN
PROCEDURE getsection_plot(f$)
  LOCAL t$,fsec,a$,b$,p$,i
  fsec=0
  IF exist(f$)
    OPEN "I",#11,f$
    WHILE not eof(#11)
      LINEINPUT #11,t$
      t$=TRIM$(t$)
      IF left$(t$)="["
        fsec=0
        s$=LEFT$(t$,LEN(t$)-1)
        s$=RIGHT$(s$,LEN(s$)-1)
        IF upper$(s$)="PLOT"
          PRINT "[PLOT]";
          FLUSH
          fsec=true
        ENDIF
      ELSE if LEFT$(t$)="#"
      ELSE if LEN(t$) AND fsec
        PRINT #1,t$
      ENDIF
    WEND
    CLOSE #11
  ENDIF
RETURN

PROCEDURE getsection_measurements(f$)
  LOCAL t$,fsec,a$,b$,p$,i
  fsec=0
  IF exist(f$)
    OPEN "I",#1,f$
    WHILE not eof(#1)
      LINEINPUT #1,t$
      t$=TRIM$(t$)
      IF left$(t$)="["
        IF fsec=2
          DEC fsec
          CLOSE #2
        ENDIF
        s$=LEFT$(t$,LEN(t$)-1)
        s$=RIGHT$(s$,LEN(s$)-1)
        IF upper$(s$)="MEASUREMENTS"
          PRINT "[MEASUREMENTS";
          FLUSH
          INC fsec
        ELSE if fsec AND UPPER$(s$)=experiment$
          PRINT "]";
          FLUSH
          INC fsec
          OPEN "O",#2,experiment$+"-pumps.dat"
        ENDIF
      ELSE if LEFT$(t$)="#"
      ELSE if fsec=2 AND LEN(t$)
        PRINT #2,t$
      ENDIF
    WEND
    CLOSE #1
    IF fsec=2
      CLOSE #2
    ENDIF
  ENDIF
RETURN
FUNCTION get_value$(m$,l$)
  LOCAL a$,b$
  WHILE LEN(m$)
    SPLIT m$," ",1,a$,m$
    IF LEN(a$)
      SPLIT a$,"=",1,a$,b$
      IF UPPER$(a$)=l$
        RETURN b$
      ENDIF
    ENDIF
  WEND
  RETURN ""
ENDFUNCTION
FUNCTION get_event$(e$)
  LOCAL fsec,t$,b$,s$,name$
  OPEN "I",#44,inffile$
  fsec=0
  WHILE NOT EOF(#44)
    LINEINPUT #44,t$
    t$=TRIM$(t$)
    IF LEFT$(t$)="["
      fsec=0
      s$=LEFT$(t$,LEN(t$)-1)
      s$=RIGHT$(s$,LEN(s$)-1)
      IF UPPER$(s$)="EVENTS"
        fsec=TRUE
      ENDIF
    ELSE IF LEN(t$)
      IF fsec
        SPLIT t$,":",1,name$,b$
        IF name$=e$
          CLOSE #44
          b$=REPLACE$(b$," = ","=")
          b$=REPLACE$(b$,","," ")
          b$=TRIM$(b$)
          ecomment$=@get_value$(b$,"COMMENT")
          RETURN @get_value$(b$,"ACTION")
        ENDIF
      ENDIF
    ENDIF
  WEND
  CLOSE #44
  RETURN ""
ENDFUNCTION
FUNCTION get_eventnr()
  LOCAL fsec,t$,b$,s$,name$,count
  count=0
  OPEN "I",#44,inffile$
  fsec=0
  WHILE NOT EOF(#44)
    LINEINPUT #44,t$
    t$=TRIM$(t$)
    IF LEFT$(t$)="["
      fsec=0
      s$=LEFT$(t$,LEN(t$)-1)
      s$=RIGHT$(s$,LEN(s$)-1)
      IF UPPER$(s$)="EVENTS"
        fsec=TRUE
      ENDIF
    ELSE IF LEN(t$)
      IF fsec
        SPLIT t$,":",1,name$,b$
        IF LEFT$(name$)=LEFT$(experiment$)
          INC count
        ENDIF
      ENDIF
    ENDIF
  WEND
  CLOSE #44
  RETURN count
ENDFUNCTION
